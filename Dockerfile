FROM tensorflow/tensorflow:latest-gpu

ENV DEBIAN_FRONTEND=noninteractive

# NodeJS installation (required by Jupyter)
RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        curl \
        gnupg; \
    curl -sL https://deb.nodesource.com/setup_12.x  | bash -; \
    apt-get install -y --no-install-recommends \
        nodejs; \
    rm -rf /var/lib/apt/lists/*

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        # OpenCV depndencies
        libgl1-mesa-glx \
        libsm6 \
        libxext6 \
        libxrender1 \
        python3-tk; \
    rm -rf /var/lib/apt/lists/*

COPY ./requirements.txt /tmp/requirements.txt

RUN pip install --no-cache-dir -r /tmp/requirements.txt


RUN set -ex; \
    jupyter labextension install @axlair/jupyterlab_vim; \
    jupyter labextension install @yeebc/jupyterlab_neon_theme


ARG DOCKER_USER

WORKDIR /home/workspace/app

RUN chown -R $DOCKER_USER /home/workspace

USER $DOCKER_USER

