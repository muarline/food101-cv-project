up:
	docker-compose build --build-arg DOCKER_USER=$$(id -u):$$(id -g)
	DOCKER_USER=$$(id -u):$$(id -g) docker-compose up -d
