from tensorflow.keras.applications import (
    EfficientNetB0,
    EfficientNetB1,
    EfficientNetB2,
    EfficientNetB3,
    EfficientNetB4,
    EfficientNetB5,
    EfficientNetB6,
    EfficientNetB7,
)
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Dense,
    Dropout,
    Concatenate,
    GlobalAveragePooling2D,
    GlobalMaxPooling2D,
    BatchNormalization
)
from tensorflow.keras.optimizers import Adam
import re


VALID_ARCH = {
    'B0': EfficientNetB0,
    'B1': EfficientNetB1,
    'B2': EfficientNetB2,
    'B3': EfficientNetB3,
    'B4': EfficientNetB4,
    'B5': EfficientNetB5,
    'B6': EfficientNetB6,
    'B7': EfficientNetB7,
}


def build_model(arch='B0', num_classes=101):
    assert arch in VALID_ARCH

    model_class = VALID_ARCH[arch]
    # Build base model
    base_model = model_class(
        include_top=False,
        weights='imagenet',
    )
    # Set initially to be untrainable
    base_model.trainable = False
    # Create classification layers
    x = base_model.output
    avg_p = GlobalAveragePooling2D()(x)
    max_p = GlobalMaxPooling2D()(x)
    x = Concatenate()([max_p, avg_p])
    x = BatchNormalization()(x)
    x = Dropout(0.3)(x)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.3)(x)
    preds = Dense(num_classes, activation='sigmoid', name='predictions')(x)
    # Build model itself
    model = Model(inputs=base_model.input, outputs=preds)
    # Compile the model
    model.compile(
        optimizer=Adam(learning_rate=1e-3),
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model


def _get_model_blocks(model):
    # Compile pattern to find efficientnet blocks
    p = re.compile('^(?:block[a-z0-9]{2}|top|stem)')
    blocks = []
    for layer in model.layers:
        name = layer.name
        name = p.findall(name)
        if len(name) == 0:
            continue
        name = name[0]
        if name not in blocks:
            blocks.append(name)
    return blocks[::-1]


def unfreeze(model, step):
    """
    Function to fine-tune a model by iteratively unfreezing layers
    """
    blocks = _get_model_blocks(model)
    current_block = blocks[step]
    print(f'Unfreezing : {current_block}')
    # Naively iterate over layers to get last non-trainable
    for layer in model.layers[::-1]:
        turn_trainable = (
            not layer.trainable
            and layer.name.startswith(current_block)
            and not isinstance(layer, BatchNormalization)
        )
        if turn_trainable:
            layer.trainable = True
    if step > 4:
        lr = 1e-4
    else:
        lr = 1e-3
    model.compile(
        optimizer=Adam(learning_rate=lr),
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model
