import os
import numpy as np

from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint

from .data import load_dataset
from .model import build_model, unfreeze
from .visualization import (
    save_histories,
    generate_confusion_matrix,
    draw_confusion_matrix,
    save_roc_auc,
)


BASE_PATH = '/home/workspace/app/experiments'
INPUT_SHAPE = {
    'B0': (224, 224),
    'B1': (240, 240),
    'B2': (260, 260),
    'B3': (300, 300),
    'B4': (380, 380),
    'B5': (456, 456),
    'B6': (528, 528),
    'B7': (600, 600),
}


def train_model(arch='B0', batch_size=64, unfreeze_steps=5):
    assert arch in INPUT_SHAPE
    # Load dataset
    train_ds, test_ds = load_dataset(INPUT_SHAPE[arch], 32)
    # train_ds, test_ds = train_ds.take(30), test_ds.take(30)
    # Create experiment dir
    experiment_path = f'{BASE_PATH}/{arch}'
    os.mkdir(experiment_path)
    # Build model
    model = build_model(arch, num_classes=101)
    # Create all histories of finetuning
    histories = []
    # Fit the model
    for step in range(unfreeze_steps):
        callbacks = [EarlyStopping(patience=2, restore_best_weights=True)]
        hist = model.fit(
            train_ds,
            epochs=1000,
            callbacks=callbacks,
            validation_data=test_ds,
        )
        histories.append(hist)
        model = unfreeze(model, step)
    # Save the model for future
    model.save(f'{experiment_path}/model.h5')
    # Save history of training
    save_histories(histories, experiment_path)
    # Save confusion matrix
    y_true = []
    for i in test_ds:
        y_true.extend(list(i[1].numpy()))
    y_true = np.array(y_true)
    probas = model.predict(test_ds)
    for top in [1, 5]:
        cm, labels = generate_confusion_matrix(
            y_true,
            probas,
            np.array(list([str(i + 1)for i in range(101)])),
            top=top,
        )
        figure = draw_confusion_matrix(cm, labels)
        figure.savefig(f'{experiment_path}/top{top}.png', facecolor='white')
    save_roc_auc(y_true, probas, experiment_path)
