import tensorflow as tf

__all__ = [
    'apply_augmentations',
    'preprocess_image'
]


def _gaussian_kernel(kernel_size, sigma, n_channels, dtype):
    """
    Generate gausiian kernel
    """
    x = tf.range(-kernel_size // 2 + 1, kernel_size // 2 + 1, dtype=dtype)
    g = tf.math.exp(-(tf.pow(x, 2) / (2 * tf.pow(tf.cast(sigma, dtype), 2))))
    g_norm2d = tf.pow(tf.reduce_sum(g), 2)
    g_kernel = tf.tensordot(g, g, axes=0) / g_norm2d
    g_kernel = tf.expand_dims(g_kernel, axis=-1)
    return tf.expand_dims(tf.tile(g_kernel, (1, 1, n_channels)), axis=-1)


def apply_gaussian_blur(img: tf.Tensor) -> tf.Tensor:
    """
    Applies gaussian blur to the image

    Gaussian kernel with k_size=3, sigma=2

    Parameters
    ----------
    img : tf.Tensor
        Image to apply blur on

    Returns
    -------
    img : tf.Tensor:
        Blured image
    """
    blur = _gaussian_kernel(3, 2, 3, img.dtype)
    img = tf.nn.depthwise_conv2d(img[None], blur, [1, 1, 1, 1], 'SAME')
    return img[0]


def preprocess_image(image: tf.Tensor, target_size=(224, 224)) -> tf.Tensor:
    """
    Resize image and apply gaussian blur

    Parameters
    ----------
    image : tf.Tensor
        Image to be resized
    target_size : (int, int)
        Target size to wich image is resized

    Returns
    -------
    image : tf.Tensor
        Resized image
    """
    image = tf.image.resize(image, target_size)
    image = apply_gaussian_blur(image)
    return image


def apply_augmentations(image: tf.Tensor) -> tf.Tensor:
    """
    Applies augmentations on image

    Applies random flip augmentations for an image

    Parameters
    ----------
    image : tf.Tensor
        Image to apply augmentations

    Returns
    -------
    image : tf.Tensor:
        Augmented image
    """
    image = tf.image.random_flip_left_right(image)
    image = tf.image.random_flip_up_down(image)
    return image
