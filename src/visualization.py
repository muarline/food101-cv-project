import csv
import numpy as np
from scipy import interp
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve, auc


def generate_confusion_matrix(y_true, probas, labels,
                              normalize=True, sort=True, top=1):
    """
    Generate confusion matrix. Can be normolized and
    sorted by accuracy. Top parameter can consider
    several top predictions.

    Parameters
    ----------
    y_true : np.ndarray, dtype=int
        Array containing true labels.
        Should be 1d and with int data type.
        Example:
            [1, 3, 4, 2, 0, 0, ..., 1, 2]
    probas : np.ndarray, dtype=float
        Array with vectors of probabilities for
        each class. Should be 2d and with float data type.
        Example:
        [
            [0.1, 0.3, 0.4, 0.2],
            [0.2, 0.5, 0.1, 0.2],
            ...
            [0.5, 0.1, 0.1, 0.3]
        ]
    labels: np.ndarray
        Array with labels
    normalize : bool, optional, default=True
        Wheather to normalize the matrix
    sort : bool, optional, default=True
        Wheather to sort matrix by dioganal values
    top : int, default=1
        Consider top N classes of predictor as True ones

    Returns
    -------
    cm : np.ndarray
        Confusion matrix with shape (labels, labels)
        and dtype=float if normalize=True or dtype=int
        if normalize=False
    labels : np.ndarray
        Labels of current matrix, will be sorted if
        sorted=True
    """
    y_pred = probas.argsort(axis=1)[..., -top:][..., ::-1]
    cm = np.zeros(shape=(len(labels), len(labels)))
    for true, pred in zip(y_true, y_pred):
        if true in pred:
            cm[true, true] += 1
        else:
            cm[true, pred[0]] += 1
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    if sort:
        sorted_index = np.argsort(cm.diagonal())[::-1]
        cm = cm[:, sorted_index][sorted_index, :]
        labels = labels[sorted_index]
    return cm, labels


def draw_confusion_matrix(cm, classes,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    Generates image of confusion matrix and returns
    it as a figure

    Parameters
    ----------
    cm : np.ndarray
        Confusion matrix to be drawn,
        shape should be (len(classes), len(classes))
    classes : np.ndarray
        Classes(labels) to be printed
    title : str, optional
        Title to write on top of the
        figure
    cmap : int, default=plt.cm.Blues
        Color map to use for drawing
        the matrix

    Returns
    -------
    figure : plt.figure
        Figure with the confusion matrix
    """
    fig, ax = plt.subplots(figsize=(60, 60))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Fixes visualisation
    offset = .5
    ax.set_ylim(cm.shape[0] - offset, -offset)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, f'{cm[i, j]:.2f}',
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    fig.set_facecolor('white')
    return fig


def save_histories(histories, base_path):
    epochs = []
    t_loss = []
    v_loss = []
    t_acc = []
    v_acc = []
    # Parse histories
    for h in histories:
        history = h.history
        t_loss.extend(history['loss'][:-5])
        t_acc.extend(history['accuracy'][:-5])
        v_loss.extend(history['val_loss'][:-5])
        v_acc.extend(history['val_accuracy'][:-5])
        epochs.append(len(history['loss'][:-5]))

    epochs = [sum(epochs[:i + 1]) for i in range(len(epochs))]
    with open(f'{base_path}/history.csv', 'w') as f:
        fieldnames = ['loss', 'accuracy', 'val_loss', 'val_accuracy']
        writer = csv.writer(f)
        writer.writerow(fieldnames)
        writer.writerows(zip(t_loss, t_acc, v_loss, v_acc))

    with open(f'{base_path}/epochs.txt', 'w') as f:
        f.writelines(list(map(lambda x: f'{x}\n', epochs)))

    # Plot results of training
    figure = plt.figure(figsize=(20, 20), facecolor='white')

    # First subplot
    figure.add_subplot(2, 1, 1)
    plt.title('Loss history')
    x = np.arange(1, len(t_loss) + 1)
    plt.plot(x, t_loss, color='orange')
    plt.plot(x, v_loss, color=(0, 0.5, 0.5))
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    for line in epochs:
        plt.axvline(line, color=(.0, 0.5, .0, 0.7), linestyle='dashed')
    plt.legend(['train_loss', 'val_loss'])
    # Second subplot
    figure.add_subplot(2, 1, 2)
    plt.title('Accuracy history')
    x = np.arange(1, len(t_acc) + 1)
    plt.plot(x, t_acc, color='orange')
    plt.plot(x, v_acc, color=(0, 0.5, 0.5))
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    for line in epochs:
        plt.axvline(line, color=(.0, 0.5, .0, 0.7), linestyle='dashed')
    plt.legend(['train_accuracy', 'val_accuracy'])

    figure.savefig(
        f'{base_path}/history.png',
        facecolor=figure.get_facecolor(),
    )


def save_roc_auc(y_true, probas, base_path, n_classes=101):
    # One hot true classes
    one_hot = np.zeros((y_true.size, n_classes))
    one_hot[np.arange(y_true.size), y_true] = 1
    y_true = one_hot
    #
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_true[:, i], probas[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= n_classes

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    # Plot all ROC curves
    figure = plt.figure(figsize=(20, 20), facecolor='white')
    figure.add_subplot(1, 1, 1)
    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average ROC curve (area = {0:0.2f})'
                   ''.format(roc_auc["macro"]),
             color='navy', linewidth=4)

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc="lower right")

    figure.savefig(
        f'{base_path}/roc_auc.png',
        facecolor=figure.get_facecolor(),
    )
