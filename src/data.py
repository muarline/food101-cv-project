import tensorflow_datasets as tfds
from tensorflow.data.experimental import AUTOTUNE

from .utils import preprocess_image, apply_augmentations


__all__ = [
    'load_dataset'
]


def load_dataset(target_img_size=(224, 224), batch_size=64):
    """
    Load Food101 dataset as tf.data.Dataset, preprocess them and
    apply augmentations for training ones

    Parameters
    ----------
        target_img_size : tuple[int, int]
            Target image size
        batch_size : int
            Batch size of returned datasets

    Returns
    -------
        train_ds : tf.data.Dataset
            Batched train dataset
        test_ds : tf.data.Dataset
            Batched test dataset
    """
    # Download Food101 tfds
    food101 = tfds.image.Food101(
        data_dir='/home/workspace/app/tensorflow_datasets'
    )
    food101.download_and_prepare()
    # Split to test and train
    datasets = food101.as_dataset()
    train_ds, test_ds = datasets['train'], datasets['validation']
    # Parse ds
    train_ds = train_ds.map(
        lambda item: (item['image'], item['label']),
        num_parallel_calls=AUTOTUNE
    )
    test_ds = test_ds.map(
        lambda item: (item['image'], item['label']),
        num_parallel_calls=AUTOTUNE
    )
    # Basic preprocessing
    train_ds = train_ds.map(
        lambda x, y: (preprocess_image(x), y),
        num_parallel_calls=AUTOTUNE
    )
    test_ds = test_ds.map(
        lambda x, y: (preprocess_image(x), y),
        num_parallel_calls=AUTOTUNE
    )
    # Augmentations for training ds
    train_ds = train_ds.map(
        lambda x, y: (apply_augmentations(x), y),
        num_parallel_calls=AUTOTUNE
    )
    # Batching
    train_ds = train_ds.batch(batch_size)
    test_ds = test_ds.batch(batch_size)
    # Prefetch for faster training
    train_ds = train_ds.prefetch(10)
    test_ds = test_ds.prefetch(10)
    return train_ds, test_ds
